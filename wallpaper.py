import arcade
from arcade.experimental import Shadertoy

from sky_grad import get_insert_code

# Do the math to figure out our screen dimensions
# SCREEN_WIDTH = 1920
# SCREEN_HEIGHT = 1080

SCREEN_WIDTH = 960
SCREEN_HEIGHT = 540
SCREEN_TITLE = "ShaderToy Demo"


class MyGame(arcade.Window):

    def __init__(self, width, height, title):
        super().__init__(width, height, title, resizable=True)
        self.time = 0
        # file_name = "fractal_pyramid.glsl"
        # file_name = "cyber_fuji_2020.glsl"
        filename = "wallpaper.glsl"
        out_filename = "wallpaper_out.glsl"
        # file_name = "flame.glsl"
        # file_name = "star_nest.glsl"
        with open(filename) as file:
            code = file.read()
            shader_sourcecode = code.replace('// COLOR_FUNC', get_insert_code())
        with open(out_filename, 'w') as file:
            file.write(shader_sourcecode)
        size = width, height
        self.shadertoy = Shadertoy(size, shader_sourcecode)
        self.mouse_pos = 0, 0

    def on_draw(self):
        arcade.start_render()
        self.shadertoy.render(time=self.time, mouse_position=self.mouse_pos)
        hour = round((self.time) % 24)
        print(f'Hour : {hour}  ', end='\r')

    def on_update(self, dt):
        # Keep track of elapsed time
        self.time += dt

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        self.mouse_pos = x, y


if __name__ == "__main__":
    MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    arcade.run()
