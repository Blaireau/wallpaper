import matplotlib.pyplot as plt
import numpy as np
from math import sin, floor

forme = lambda x: 0.5*sin(3*x+1.5)**2

# montagnes en vagues 
amp = lambda x: 0.4*x**3 + (x-0.5)**2 + 0.5
ligne = lambda x: 0.5*sin(9*x+0.5)**2 + 0.4
mont = lambda x: amp(x)*ligne(x)

# montagne simple pointe 
amp = lambda x: 0.4*x**3 + (x-0.5)**2 + 0.5

def rand(seed, low, high):
    return low + seed % (high - low)

def randsgn(seed):
    return 1 if seed % 2 == 0 else -1

def wave(center, amp, freq):
    return lambda x: amp*sin((x-center)*freq*4**-abs(x-center))
def exp(hauteur, centre, drop):
    return lambda x: hauteur*(2**-drop)**abs(x-centre)

def pointe(hauteur, centre, dropA, dropB):
    # return lambda x: ( hauteur-abs(x-centre)*base**abs(x-centre) )
    return lambda x: exp(hauteur, centre, dropA)(x) if x < centre else exp(hauteur, centre, dropB)(x)

def mountain(seed):
    dropA = rand(seed, 1, 10)
    dropB = rand(3*seed, 1, 10)
    fshift = rand(5*seed, 50, 100)
    line = pointe(0.7, 0.5, dropA, dropB)
    noise = wave(0.5, 0.01, fshift)
    return lambda x: line(x)+noise(x)

inc = lambda y: 4**(-abs(1-y))

sinc = lambda x : sin(x) / x if x != 0 else 1

def split_noise(seed):
    f1 = rand(7*seed, 30, 60) / 10
    f2 = rand(11*seed, 200, 400) / 10
    f3 = rand(17*seed, 400, 800) / 10
    c2 = rand(19*seed, 10, 60) / 100
    c3 = rand(23*seed, 10, 60) / 100
    a1 = 0.3*randsgn(29*seed)
    a2 = 0.1*randsgn(31*seed)
    a3 = 0.1*randsgn(37*seed)
    return lambda y: a1*sinc(f1*y-3) + a2*sinc(f2*(y-c2)) + a3*sinc(f3*(y-c3))


def split_wave(seed):
    return lambda y: -0.3*sinc(11*y-3)


def mountain_split(height, seed):
    return lambda y: split_noise(seed)(y-height)

def rand_bin(x):
    x = x + 0.9
    offset = 0.3
    f1 = (x % 0.2) / 0.2 - offset
    f2 = (x % 0.3) / 0.3 - offset
    y = max(round(f1), round(f2))
    return y

ligne = pointe(0.7, 0.5, 1, 8) 

random = lambda x: (1+sin(12345.4321*x))/4
escalier = lambda x: max(sin(20*x)**2, sin(8*x+0.5)**2)
mont = lambda x: amp(x)*ligne(x)

f = 8
xa = lambda x: floor(x*f)/f
xb = lambda x: xa(x) + 1/f
ya = lambda x: random(xa(x)) + 0.3
yb = lambda x: random(xb(x)) + 0.3


# l1 = lambda x: max(forme(x), mont(x))
# l1 = lambda x: ya(x) + (yb(x) - ya(x))*f*(x-xa(x)) + 0.01*sin(80*x)
l1 = lambda x: ligne(x)

final_func = rand_bin


xv = np.linspace(0, 1, 1000)
yv = np.vectorize(final_func)(xv)

plt.plot(xv, yv)
# plt.plot(xv, 0*xv)
# plt.vlines(0.5, -1, 1)
plt.axis([0, 1, -1.05, 1])
plt.show()
