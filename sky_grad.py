from PIL import Image

IF_BLOC = """if (hour < {hour}.0) {{
    color.rgb = vec3({color});
}}"""

SEP = ' else '

FUNCTION_BODY = """vec3 {funcName}(float hour) {{
    vec3 color = vec3(0, 0, 0);
    {funcBody}
    return(color);
}}"""

def get_palette():
    return list(_get_palette())

def _get_palette():
    im = Image.open('palette.png')
    for x in range(0, 1200, 50):
        top = im.getpixel((x, 0))
        bot = im.getpixel((x, 49))
        yield top, bot

def decode_rgb(color):
    color = [int(color[i:i+2], 16) for i in range(1, 7, 2)]
    return format_rgb(color)

def format_rgb(color):
    r, g, b, *reste = [c/256 for c in color]
    return f'{r}, {g}, {b}'

def get_big_color_if(palette, position):
    lines = []
    for hour, colors in enumerate(palette):
        lines.append(IF_BLOC.format(hour=hour+1,
                                    color=format_rgb(colors[position])))
    out = SEP.join(lines)
    return out

def get_function(name, palette, position):
    body = get_big_color_if(palette, position)
    return FUNCTION_BODY.format(funcName=name, funcBody=body)

def get_insert_code():
    palette = get_palette()
    code = []
    for pos, name in enumerate(['top_color', 'bottom_color']):
        funcCode = get_function(name, palette, pos)
        code.append(funcCode)
    return "\n".join(code)
