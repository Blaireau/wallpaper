#define WHITE vec3(1, 1, 1) 
#define GREEN vec3(0, 1, 0) 


// COLOR_FUNC

///////////////////////////////////////////////////////////////////////
//                     MOUNTAIN GENERATION
///////////////////////////////////////////////////////////////////////
#define DROP_MIN 1
#define DROP_MAX 10
#define F_MIN 50
#define F_MAX 100
#define NOISE_AMP 0.01

#define MOUNTAIN_DARK vec3(0.01, 0.01, 0.01)
#define MOUNTAIN_LIGHT vec3(0.2, 0.2, 0.2)

float rand(int seed, float low, float high){
    return(low + mod(seed, high - low));
}

float randsgn(int seed){
    if (mod(seed, 2) == 0){
        return 1.0;
    } else {
        return -1.0;
    }
}

float wave(float x, float center, float amp, float freq){
    return(amp*sin((x-center)*freq*pow(4, -abs(center-x))));
}

float exp_side(float x, float height, float center, float drop){
    return(height*pow(pow(2, -drop), abs(x-center)));
}

float peak(float x, float height, float center, float dropA, float dropB){
    if (x < center){
        return(exp_side(x, height, center, dropA));
    } else {
        return(exp_side(x, height, center, dropB));
    }
}

float sinc(float x){
    return(sin(x)/x);
}

float mountain_split(float y, int seed, float height, float center, float minDrop){
    float f1 = rand(7*seed, 30, 60) / 10;
    float f2 = rand(11*seed, 200, 400) / 10;
    float f3 = rand(17*seed, 400, 800) / 10;
    float c2 = rand(19*seed, 10, 60) / 100;
    float c3 = rand(23*seed, 10, 60) / 100;
    float a1 = 0.2*randsgn(29*seed);
    float a2 = 0.1*randsgn(31*seed);
    float a3 = 0.1*randsgn(37*seed);
    y = y - height;
    return(center + a1*sinc(f1*y-3) + a2*sinc(f2*(y-c2)) + a3*sinc(f3*(y-c3)));
}

float mountain_shape(float x, int seed, float height, float center, out float minDrop){
    float dropA = rand(seed, DROP_MIN, DROP_MAX);
    float dropB = rand(3*seed, DROP_MIN, DROP_MAX);
    minDrop = min(dropA, dropB);
    float freq = rand(5*seed, F_MIN, F_MAX);
    float line = peak(x, height, center, dropA, dropB);
    float noise = wave(x, center, NOISE_AMP, freq);
    return(line + noise);
}

void draw_mountain(float x, float y, int seed, float height, float center, int invert, inout vec3 color, inout bool stop){
    seed++;
    if (stop){
        return;
    } 
    float minDrop;
    if (y < mountain_shape(x, seed, height, center, minDrop)){
        bool test = x < mountain_split(y, seed, height, center, minDrop);
        if (invert == 1){
            test = !test;
        }
        if (test){
            color = MOUNTAIN_DARK;
        } else {
            color = MOUNTAIN_LIGHT;
        }
        stop = true;
    }
}

bool draw_mountains(float x, float y, out vec3 color){
    bool stop = false;

    // central mountain
    draw_mountain(x, y, 2028, 0.7, 0.5, 1, color, stop);

    // right side mountains
    draw_mountain(x, y, 1333, 0.6, 0.85, 1, color, stop);
    draw_mountain(x, y, 1342, 0.6, 0.9, 1, color, stop);
    draw_mountain(x, y, 1355, 0.6, 0.7, 1, color, stop);

    // left side mountains
    draw_mountain(x, y, 2017, 0.6, 0.15, 0, color, stop);
    draw_mountain(x, y, 2001, 0.6, 0.3, 0, color, stop);
    draw_mountain(x, y, 2014, 0.6, 0.25, 0, color, stop);
    draw_mountain(x, y, 2021, 0.6, 0.05, 0, color, stop);
    return stop;
}

///////////////////////////////////////////////////////////////////////
//                    TREE GENERATION
///////////////////////////////////////////////////////////////////////
#define TRUNK_COLOR vec3(0.1, 0.1, 0.1)
#define BRANCH_COLOR vec3(0.3, 0.2, 0.2)

void draw_branch(vec2 point, inout vec3 color, vec2 start, vec2 dir, 
                 float branchLength, float startWidth, float endWidth,
                 float amp, float freq){
    vec2 a = point - start;
    float factor = dot(dir, a) / dot(dir, dir);
    vec2 proj = factor*dir;
    float u = length(proj);
    if (u > branchLength  || factor < 0.0){
        return;
    }
    vec2 vVec = a-proj;
    float v = length(vVec)*sign(dot(vec2(proj.y, -proj.x), vVec));
    float width = mix(startWidth, endWidth, u / branchLength);   
    float inside = abs(v+amp*sin(freq*u)) - width;
    color = mix(BRANCH_COLOR, color, smoothstep(-0.005, 0.005, inside));
}

void draw_tree(vec2 pos, inout vec3 color, vec2 treeDir, float treeHeight){
    vec2 relPos = pos - vec2(0.8, 0.05);
    draw_branch(relPos, color, vec2(0, 0), treeDir, treeHeight, 0.05, 0.01, 0.01, 10);
    for (int br = 0; br < 6; br++){
        float sgn = randsgn(2000+br);
        vec2 dir = treeDir + vec2(sgn*0.5, 0);
        vec2 start = treeDir*treeHeight*mix(0.1, 0.7, br/6.0);
        float height = treeHeight*rand(br*123, 15, 80)/100.0;
        draw_branch(relPos, color, start, dir, height, 0.02, 0.01, 0.01, 10);
    }
}

///////////////////////////////////////////////////////////////////////
//                    MAIN PROGRAMM 
///////////////////////////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 pos = fragCoord;
    pos.x = pos.x / iResolution.y;
    pos.y = pos.y / iResolution.y;

    float distortedTime = iTime;

    float hour = mod(distortedTime, 24.0);
    float windowTime = mod(distortedTime, 1);
    vec3 topColor = mix(top_color(hour - 1), top_color(hour), windowTime);
    vec3 bottomColor = mix(bottom_color(hour - 1), bottom_color(hour), windowTime);


    float a = pos.y; 
    vec3 color = a*topColor.rgb + bottomColor.rgb*(1-a);

    vec3 mountainColor = vec3(0, 0, 0);
    bool m = draw_mountains(pos.x, pos.y, mountainColor);
    if (m){
        vec3 blendColor = bottomColor + topColor;
        float luminosity = clamp(length(blendColor)/2 - 0.2, 0, 1);
        mountainColor = mix(MOUNTAIN_DARK, mountainColor, luminosity); 
        color = (blendColor + 3*mountainColor)/4;
    }
    /* if (abs(pos.y-0.6) < 0.001 || abs(pos.x-0.5) < 0.001){ */
    /*     color = WHITE; */
    /* } */
    if (pos.y < 0.3){
        color = MOUNTAIN_DARK;
    }


    color = vec3(0.2, 0.7,0.9);

    draw_tree(pos, color, vec2(0, 1), 0.6);
    




    fragColor = vec4(color, 1);
}
