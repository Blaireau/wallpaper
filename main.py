import arcade
from arcade.experimental.texture_render_target import RenderTargetTexture

from math import floor

from sky_grad import get_palette

DEBUG = False

SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
# SCREEN_WIDTH = 960
# SCREEN_HEIGHT = 540

SCREEN_TITLE = "Shader in python"

if DEBUG:
    debug = print
else:
    debug = lambda *args, **kwargs: None

def read_file(filename):
    with open(filename) as file:
        content = file.read()
    return content

def lerp(vecA, vecB, mix):
    return [(vecA[i]*(1-mix) + vecB[i]*mix)/255 for i in range(3)]


class RandomFilter(RenderTargetTexture):
    def __init__(self, width, height):
        super().__init__(width, height)

        filename = "fragment_shader.glsl"
        # filename = "minimal_bug.glsl"
        out_filename = "fragment_out.glsl"
        code = read_file(filename)
        # shader_sourcecode = code.replace('// COLOR_FUNC', get_insert_code())
        # with open(out_filename, 'w') as file:
        #     file.write(shader_sourcecode)
        size = width, height
        self.program = self.ctx.program(
            vertex_shader=read_file('vertex_shader.glsl'),
            fragment_shader=code,
        )
        self.set_uniform('iTime', 0.0)
        self.set_uniform('iResolution', (SCREEN_WIDTH, SCREEN_HEIGHT))
        self.set_uniform('topColor', (0, 0, 0))
        self.set_uniform('bottomColor', (1, 1, 1))

    def set_uniform(self, key, val):
        try:
            self.program[key] = val
        except KeyError:
            debug(f'no uniform : {key}')
            pass

    def use(self):
        self._fbo.use()

    def draw(self):
        self.texture.use(0)
        self._quad_fs.render(self.program)

    def update(self, time, topColor, bottomColor):
        self.set_uniform('iTime', time)
        self.set_uniform('topColor', topColor)
        self.set_uniform('bottomColor', bottomColor)


class MyGame(arcade.Window):

    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        self.time = 0
        self.filter = RandomFilter(width, height)
        self.palette = get_palette()

    def on_draw(self):
        self.clear()
        # self.filter.clear()
        # self.filter.use()
        # self.use()
        self.filter.draw()
        # arcade.draw_circle_filled(self.width / 2, self.height / 2, 100, arcade.color.RED)
        hour = round((self.time) % 24)
        print(f'Hour : {hour}  ', end='\r')


    def on_update(self, dt):
        self.time += dt
        hour = floor(self.time % 24)
        mix = self.time % 1
        t1, b1 = self.palette[hour-1]
        t2, b2 = self.palette[hour]
        topColor = lerp(t1, t2, mix)
        bottomColor = lerp(b1, b2, mix)

        self.filter.update(self.time, topColor, bottomColor)

    def on_key_press(self, key, modifiers):
        if key == arcade.key.Q:
            self.close()


game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
arcade.run()
